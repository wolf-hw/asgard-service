const config = {
  server: 'http://api.alpha.saas.hand-china.com',
  master: '@choerodon/master',
  projectType: 'choerodon',
  buildType: 'single',
  dashboard: {},
  resourcesLevel: ['site', 'organization', 'project', 'user'],
};

module.exports = config;
